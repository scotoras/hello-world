package demo;

public class App {
    static void counter(int i) {
        while(i >= 0) {
            System.out.println("Counter: " + i);

            try {

                Thread.sleep(5000);

            } catch (InterruptedException e) {
                System.err.format("IOException: %s%n", e);
            }

            i++;
        }
    }

    public static void main(String[] args){
        try {
            counter(Integer.parseInt(System.getenv("COUNTER_VALUE")));
        } catch (NumberFormatException e) {
            System.err.format("IOException: %s%n", e);
        }
    }
}
