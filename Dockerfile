FROM openjdk:8
ENV LANG C.UTF-8
WORKDIR /app
COPY ./build/libs/*.jar /app/app.jar
CMD ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom", "-Doracle.jdbc.timezoneAsRegion=false", "-jar", "/app/app.jar"]
